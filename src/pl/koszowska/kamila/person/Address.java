package pl.koszowska.kamila.person;

/**
 * Created by RENT on 2017-08-11.
 */
public class Address {
    private String street;
    private String houseNumber;
    private String flatNumer;
    private String postalcode;
    private String city;

    public Address(String street, String houseNumber, String flatNumer, String postalcode, String city) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumer = flatNumer;
        this.postalcode = postalcode;
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getFlatNumer() {
        return flatNumer;
    }

    public void setFlatNumer(String flatNumer) {
        this.flatNumer = flatNumer;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
