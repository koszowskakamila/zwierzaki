package pl.koszowska.kamila.person;

import pl.koszowska.kamila.animal.Animal;
import pl.koszowska.kamila.animal.Feed;
import pl.koszowska.kamila.animal.KindOfFeeds;

import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public class Person {

    private String name;
    private String surname;
    private int age;
    private Sex sex;
    private Address address;
    private List<Animal> myAnimals;
    private List<Integer> alergy;
    private List<KindOfFeeds> feedsForAnimals;

    public List<Integer> getAlergy() {
        return alergy;
    }

    public void setAlergy(List<Integer> alergy) {
        this.alergy = alergy;
    }

    public Person(String name, String surname, int age, Sex sex, Address address, List<Animal> myAnimals) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.sex = sex;
        this.address = address;
        this.myAnimals = myAnimals;
    }

    public void feedAnimals(){
        for (Animal animal : myAnimals) {
            List<KindOfFeeds> animalFeeds = animal.getFeeds();

            for (int i=0;i<animalFeeds.size();i++){
                KindOfFeeds kindOfFeeds = animalFeeds.get(i);
                KindOfFeeds myKindOfFeeds = doIHaveEnoughtFeed(kindOfFeeds);
                if (myKindOfFeeds != null){
                    if (myKindOfFeeds.getQuantity()<kindOfFeeds.getQuantity()) {
                        System.out.printf("Muszę dokupić " + myKindOfFeeds.getFeed() + " bo mam tylko " + myKindOfFeeds.getQuantity() + " :(");
                        buyMoreFeed(myKindOfFeeds,kindOfFeeds.getQuantity());
                    }
                }else{
                    System.out.println("Nie mam w spiżarni " + kindOfFeeds.getFeed() + ":( Idę do sklepu kupić " + kindOfFeeds.getFeed());
                    myKindOfFeeds = new KindOfFeeds(kindOfFeeds.getFeed(),kindOfFeeds.getQuantity());
                    buyFeed(myKindOfFeeds);
                }
                animal.feedMe(myKindOfFeeds.getFeed());
                myKindOfFeeds.removeFeed(kindOfFeeds.getQuantity());
                System.out.println("Nakarmiłam " + animal.getAnimalType() + " o imieniu " + animal.getName() + " " + kindOfFeeds.getFeed());


            }
        }

    }

    public boolean canHaveAnimal(Animal animal){
        for (int i=0; i<alergy.size();i++){
            Integer a = alergy.get(i);
            if (animal.getAllergy().stream().filter(alerg -> alerg.equals(a)).findAny() != null ){
                return false;
            }
        }
        return true;
    }
/*
    public boolean doIHaveFeed(KindOfFeeds kindOfFeeds){
        for (int i=0; i<feedsForAnimals.size();i++){
            if (feedsForAnimals.get(i).compareTo(kindOfFeeds) == 0){
                return true;
            }
        }
        return false;
    }
*/
    public KindOfFeeds doIHaveEnoughtFeed(KindOfFeeds kindOfFeeds){
        for (int i=0; i<feedsForAnimals.size();i++){
            if (feedsForAnimals.get(i).compareTo(kindOfFeeds) == 0){
                return feedsForAnimals.get(i);
            }
        }
        return null;
    }

    public void buyFeed(KindOfFeeds newKindOfFeeds){
        getFeedsForAnimals().add(newKindOfFeeds);
        System.out.println("Kupiłam " + newKindOfFeeds.getFeed() + ". Teraz mogę nakarmić moje zwierzątko :)");
    }

    public void buyMoreFeed(KindOfFeeds kindOfFeeds, int quantity){
        kindOfFeeds.addFeed(quantity);
        System.out.println("Dokupiłam " + kindOfFeeds.getFeed() + ". Teraz mogę nakarmić moje zwierzątko :)");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Animal> getMyAnimals() {
        return myAnimals;
    }

    public void setMyAnimals(List<Animal> myAnimals) {
        this.myAnimals = myAnimals;
    }

    public List<KindOfFeeds> getFeedsForAnimals() { return feedsForAnimals; }

    public void setFeedsForAnimals(List<KindOfFeeds> feedsForAnimals) { this.feedsForAnimals = feedsForAnimals; }

}
