package pl.koszowska.kamila.main;

import pl.koszowska.kamila.person.Address;
import pl.koszowska.kamila.person.Sex;
import pl.koszowska.kamila.animal.*;
import pl.koszowska.kamila.person.Person;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by RENT on 2017-08-16.
 */
public class Main {

    public static void main(String[] args) {

        Animal cat = new Cat();
        cat.setName("Kicia");
        cat.setAnimalType(AnimalType.cat);
        cat.setFeeds(new ArrayList<KindOfFeeds>(Arrays.asList(new KindOfFeeds(Feed.milk,1),new KindOfFeeds(Feed.meat,2))));

        Person me = new Person("Kamila","Koszowska",31, Sex.women, new Address("","","","","Lublin"),Arrays.asList(cat));

        ArrayList<KindOfFeeds> feedses = new ArrayList<KindOfFeeds>(Arrays.asList(new KindOfFeeds(Feed.milk,4),new KindOfFeeds(Feed.grain,3)));

        me.setFeedsForAnimals(feedses);

        me.feedAnimals();

    }
}
