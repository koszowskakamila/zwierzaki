package pl.koszowska.kamila.animal;

import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public abstract class Animal {

    private String name;
    private int age;
    private AnimalType animalType;
    private List<Integer> allergy;
    private List<KindOfFeeds> feeds;

    public boolean feedMe(Feed feed){
  /*      if (feeds.indexOf(feed) != -1){
            return true;
        }
        */
        return true;
    }

    public List<KindOfFeeds> getFeeds() {
        return feeds;
    }

    public void setFeeds(List<KindOfFeeds> feeds) {
        this.feeds = feeds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }

    public void setAnimalType(AnimalType animalType) {
        this.animalType = animalType;
    }

    public List<Integer> getAllergy() {
        return allergy;
    }

    public void setAllergy(List<Integer> allergy) {
        this.allergy = allergy;
    }
}
