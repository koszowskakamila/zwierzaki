package pl.koszowska.kamila.animal;

/**
 * Created by RENT on 2017-08-16.
 */
public class KindOfFeeds implements Comparable<KindOfFeeds>{

    private Feed feed;
    private int quantity;

    public KindOfFeeds(Feed feed, int quantity) {
        this.feed = feed;
        this.quantity = quantity;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void addFeed(int quantity){
        this.quantity+=quantity;
    }

    public void removeFeed(int quantity){
        if (this.quantity < quantity){
            this.quantity = 0;
        }else {
            this.quantity-=quantity;
        }
    }

    @Override
    public int compareTo(KindOfFeeds kindOfFeeds) {
        if (this.feed == kindOfFeeds.getFeed()){
            return 0;
        }else{
            return -1;
        }
//        return this.feed.toString().compareTo(feed.toString());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KindOfFeeds)) return false;

        KindOfFeeds that = (KindOfFeeds) o;

        return (getFeed() == that.getFeed());

    }



    @Override
    public int hashCode() {
        return getFeed().hashCode();
    }

/*
   @Override
    public boolean equals(Object obj){
        boolean result = false;

        if (obj instanceof KindOfFeeds) {

            KindOfFeeds kindOfFeeds = (KindOfFeeds) obj;

            if (getFeed() == kindOfFeeds.getFeed()) {
                result = true;
            }
        }

        return result;
    }
*/
}
