package pl.koszowska.kamila.animal;

/**
 * Created by RENT on 2017-08-11.
 */
public enum AnimalType {
    cat,
    dog,
    fish,
    bird
}
